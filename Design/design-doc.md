# Lenxion: The Cursed Castle Design Document

## Story

This game will be a "prequel" to the 2008 game Lenxion. In that game,
you're escaping the castle Lenxion, fighting off red "Splodeybugs",
and the game ends outdoors.

This game will be as you're trying to get *into*** Lenxion Castle to steal
the treasure. At this point in time, the main character doesn't have the
laser choker, and has to find weapons to use as she scours the castle for
treasure and valuables to steal.

The castle, however, is protected (and controlled) by a wizard, who has
transformed the king and guards into monsters.

### Characters

**Ajna** 

(Formerly spelled "Ayne")

Ayda is the main character of the 2008 Lenxion.

This character was originally named after "Ein" the dog in Cowboy Bebop,
and I only later became aware of Ayn Rand. Therefore, I am renaming
her to distance her from that person, lol. I didn't want to make that
association. :|

Ayda is a thief, and that's the extent of her backstory that I've figured out.
I assume she's an elite thief as she's targetting castles, apparently.

**???**

Evil wizard guy

## Assets

### Maps

**Area 1: Castle grounds**

1. Garden
2. Stables
3. Watchtower
4. ?

**Area 2: Castle 1st floor**

1. Kitchen
2. Throne Room
3. Ballroom
4. Library

**Area 3: Castle tower**

1. ??
2. King's Quarters
3. Wizard's Quarters
4. Treasure Room

### Art

#### Character art

* Ayda (small overworld map)
* Ayda (large room map)
  * Movement 4 directions
  * Attack animations, 4 directions, throw, swing sword, shoot lasers?
  
* Area 1 guards, 4 directions of movement, basic attack frames.
  * Goblin guard
  * Skeleton guard
  * Snake guard
  * Floor guard
* Area 2 guards (color-swapped?)
  * Floor guard
  * Spider guard
  * Two-headed guard
  * Troll guard
  * Dragon guard
* Area 3 guards (color-swapped?)
  * Floor guard
  * Genie?
  * Cyclops
  * Demon
  * Bats

#### Item art

* Collectables:
  * Key to Area 2
  * Key to Area 3
  * Treasure:
    1. Gemstone
    2. Treasure chest
    3. Gold bars
    4. Gold urn
    5. Crown
    6. Jewelry
  * Weapons:
    1. Apples
    2. Sword
    3. Laser necklace :p

#### Environment art

* Walls:
  * Outer castle wall
  * Internal castle walls
  * Wooden walls
  * Plant (bush) walls
* Floors:
  * Grass
  * Wood floor
  * Cobblestone path
  * Stone floor
  * Carpet floor
* Decor:
  * Tapestries
  * Hay bail
  * Candlesticks
  * 

#### Story art / full screen

* Comic frames for beginning / ending of game? Between floors?
* Title screen art

#### UI art

* Blank button (add text / icon)
* Button icons:
  * Start/play
  * Help
  * Options
  * Exit
* HUD frame
* Heart life indicator
* Hidden / Spotted indicator
