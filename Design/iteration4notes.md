Items to be done in Iteration 4 will be chosen later, but here is all the things to think about for now:

# Remaining features:

## Core gameplay:

1.  #27 Item location placement on map 
1.  #39 Player collect treasure / treasure view on HUD / gold points; don't show treasure on map once collected 
1.  #19 Enemy attacking player
1.  #41 Enemy location placement on map
1.  #40 Player lives / reset after hit
1.  #45 Map warping placement on map
1.  #46 Map changing during game-state
1.  #47 Whole floor / small sprites maps
1.  #54 Going to next area after all treasure on floor is collected.
1.  #13 Playing music and sound effects

## Core application:

1.  #23 Language manager, English and Esperanto support
1.  #44 Title screen with play / options / help / exit buttons
1.  #20 Configuration screen, volume control, language selection
1.  #43 Language select screen
1.  #21 Help screen / credits

## Asset creation:

1.  #16 Making music and #15 sound effects
1.  #49 Making tile art for all areas (big)
1.  #50 Making tile art for all areas (small)
1.  #51 Making player art for small areas
1.  #52 Making one type of enemy art for small areas
1.  #53 Making enemy art for all enemies
1.  #2 Making treasure art for all treasures
1.  #6 UI element art
1.  #5 Title screen art
1.  #48 Making all the maps

## Extended:

1.  Different types of enemies, with different behaviors and attack styles
1.  Different items
1.  #38 Draw order fix

## Marketing:

1.  #42 Store art

# Things to plan (on paper):

* Different map environments; tiles needed list
* Rough map designs; sketch out
* Different enemy types; sketch out
* Different treasure types; sketch out
* Title/Help/Options/Language screen layouts; sketch out wireframe

## Enemies...

* Area 1 guards, 4 directions of movement, basic attack frames.
  * Goblin guard
  * Skeleton guard
  * Snake guard
  * Floor guard
* Area 2 guards (color-swapped?)
  * Floor guard
  * Spider guard
  * Two-headed guard
  * Troll guard
  * Dragon guard
* Area 3 guards (color-swapped?)
  * Floor guard
  * Genie?
  * Cyclops
  * Demon
  * Bats

## Areas...

**Area 1: Castle grounds**

1. Garden
2. Stables
3. Watchtower
4. ?

**Area 2: Castle 1st floor**

1. Kitchen
2. Throne Room
3. Ballroom
4. Library

**Area 3: Castle tower**

1. ??
2. King's Quarters
3. Wizard's Quarters
4. Treasure Room

## Items...

* Collectables:
  * Key to Area 2
  * Key to Area 3
  * Treasure:
    1. Gemstone
    2. Treasure chest
    3. Gold bars
    4. Gold urn
    5. Crown
    6. Jewelry
  * Weapons:
    1. Apples
    2. Sword
    3. Laser necklace :p


