#include "Npc.hpp"


Npc::Npc() : Character()
{
    m_debugRectColor = sf::Color( 0, 0, 255, 100 );
    m_mapCollisionRectangle.left = 28;
    m_mapCollisionRectangle.top = 140;
    m_mapCollisionRectangle.width = 25;
    m_mapCollisionRectangle.height = 15;
    m_objectCollisionRectangle.left = 22;
    m_objectCollisionRectangle.top = 34;
    m_objectCollisionRectangle.width = 38;
    m_objectCollisionRectangle.height = 120;
    m_sightRadius = 100;
    m_speed = 1;
    ResetGoal();
}

void Npc::ResetGoal()
{
    // Goal can be used to chase the player
    m_goal.x = -1;
    m_goal.y = -1;
}

void Npc::SetGoal( sf::Vector2f position )
{
    m_goal = position;
}

chalo::Action Npc::Update( const chalo::Map::ReadableMap& gameMap, const Player& player, std::vector< Projectile >& projectiles )
{
    Character::Update();
    // Behavioral code
    float distanceFromPlayer = chalo::GetDistance( player.GetPositionRegion(), GetPositionRegion(), true );

    if ( distanceFromPlayer <= m_sightRadius )
    {
        SetGoal( player.GetPosition() );
    }

    // TODO: Generalize this
    // Check if hit by projectile
    for ( auto& projectile : projectiles )
    {
        if ( chalo::GetDistance( GetPositionRegion(), projectile.GetPositionRegion(), true ) < 20 )
        {
            m_activeState = chalo::WAITING_DELETION;
            projectile.SetActiveState( chalo::WAITING_DELETION );
        }
    }

    // Have a goal to move towards
    if ( m_goal.x != -1 )
    {
        if ( m_goal.x < m_position.x ) // Goal is to the left
        {
            Move( chalo::WEST, gameMap );
        }
        else if ( m_goal.x > m_position.x ) // Goal is to the right
        {
            Move( chalo::EAST, gameMap );
        }

        if ( m_goal.y < m_position.y ) // Goal is above
        {
            Move( chalo::NORTH, gameMap );
        }
        else if ( m_goal.y > m_position.y ) // Goal is below
        {
            Move( chalo::SOUTH, gameMap );
        }
    }

    return m_stateAction;
}
