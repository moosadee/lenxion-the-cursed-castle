#ifndef _NPC
#define _NPC

#include "../chalo-engine/GameObjects/Character.hpp"
#include "Projectile.hpp"
#include "Player.hpp"

class Npc : public chalo::Character
{
public:
    Npc();

    chalo::Action Update( const chalo::Map::ReadableMap& gameMap, const Player& player, std::vector< Projectile >& projectiles );

    void ResetGoal();
    void SetGoal( sf::Vector2f position );

protected:
    int m_sightRadius;

    sf::Vector2f m_goal;
};

#endif
