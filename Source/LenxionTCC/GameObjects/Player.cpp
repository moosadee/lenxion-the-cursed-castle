#include "Player.hpp"


Player::Player() : Character()
{
    m_debugRectColor = sf::Color( 0, 255, 0, 100 );
    m_mapCollisionRectangle.left = 0;
    m_mapCollisionRectangle.top = 0;
    m_mapCollisionRectangle.width = 0;
    m_mapCollisionRectangle.height = 0;
    m_objectCollisionRectangle.left = 0;
    m_objectCollisionRectangle.top = 0;
    m_objectCollisionRectangle.width = 0;
    m_objectCollisionRectangle.height = 0;
    m_characterType = chalo::PLAYER;
}

chalo::Action Player::Update( const chalo::Map::ReadableMap& gameMap )
{
    Character::Update();

    m_stateAction = chalo::IDLE;

    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )
    {
        Move( chalo::WEST, gameMap );
    }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )
    {
        Move( chalo::EAST, gameMap );
    }

    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )
    {
        Move( chalo::NORTH, gameMap );
    }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )
    {
        Move( chalo::SOUTH, gameMap );
    }

    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Space ) )
    {
        BeginAttack();
    }

    return m_stateAction;
}
