#ifndef _PLAYER
#define _PLAYER

#include "../chalo-engine/GameObjects/Character.hpp"

class Player : public chalo::Character
{
public:
    Player();

    chalo::Action Update( const chalo::Map::ReadableMap& gameMap );

protected:
};


#endif
