#include "Projectile.hpp"

void Projectile::Update( const chalo::Map::ReadableMap& gameMap )
{
    Character::Update();

    // Keep moving in the direction its facing.
    MoveClipping( m_direction );

    sf::IntRect positionRect;
    positionRect.left = m_position.x;
    positionRect.top = m_position.y;
    positionRect.width = m_textureCoordinates.width;
    positionRect.height = m_textureCoordinates.height;

    // Check to see if it has hit a wall
    if ( gameMap.IsCollision( positionRect, m_mapCollisionRectangle ) )
    {
        // It will be deleting by the game state later.
        m_activeState = chalo::WAITING_DELETION;
    }
}
