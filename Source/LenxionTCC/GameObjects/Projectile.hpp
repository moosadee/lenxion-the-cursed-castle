#ifndef _PROJECTILE
#define _PROJECTILE

#include "../chalo-engine/GameObjects/Character.hpp"

class Projectile : public chalo::Character
{
public:
    void Update( const chalo::Map::ReadableMap& gameMap );

private:
    bool m_hurtsPlayer;
    bool m_hurtsEnemy;
};

#endif
