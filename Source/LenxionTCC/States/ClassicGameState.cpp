#include "ClassicGameState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Debug/DebugDraw.hpp"
#include "../chalo-engine/Application/Application.hpp"

#include <fstream>
#include <algorithm>

ClassicGameState::ClassicGameState()
{
}

void ClassicGameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "ClassicGameState::Init", "function-trace" );
    IState::Init( name );
}

void ClassicGameState::Setup()
{
    chalo::Logger::Out( "", "ClassicGameState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "background",   "Content/Classic/background.png" );
    chalo::TextureManager::Add( "tileset",      "Content/Classic/tileset.png" );
    chalo::TextureManager::Add( "ayda",         "Content/Classic/ayda.png" );

    chalo::InputManager::Setup();

    // Camera
    m_camera.SetPosition( 0, 0 );
    m_camera.SetDimensions( chalo::Application::GetScreenWidth(), chalo::Application::GetScreenHeight() );
    m_camera.SetRegionBound( sf::IntRect( 0, 0, 2000, 2000 ) );

    // Whee
    m_backgroundImage.setTexture( chalo::TextureManager::Get( "background" ) );
    m_backgroundImage.setPosition( 0, 0 );

    // Set up background
//    m_map.LoadFile( "Content/Classic/", "dungeon.map", chalo::TextureManager::Get( "tileset" ) );
    m_map.LoadFile( "Content/Classic/", "castle.map", chalo::TextureManager::Get( "tileset" ) );
    m_map.SetSolidTiles( { 1, 5, 9, 39, 42, 45 } );

    m_ayda.Setup( 0, 0, "ayda" );
    m_ayda.SetTexture( chalo::TextureManager::Get( "ayda" ), 0, 0, 32, 48 );
    m_ayda.SetPosition( 50, 375 );
    m_ayda.SetAnimationInformation( 4, 0.15 );
    m_ayda.SetObjectCollisionRectangle( sf::IntRect( 7, 4, 17, 42 ) );
    m_ayda.SetMapCollisionRectangle( sf::IntRect( 7, 4, 17, 42 ) );
    m_ayda.gravity.SetGravity( -1 );
    m_ayda.gravity.SetIsFalling( false );
}

void ClassicGameState::Cleanup()
{
    chalo::Logger::Out( "", "ClassicGameState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void ClassicGameState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    m_ayda.Update();

    // Check gravity
    if ( !m_map.IsCollision( m_ayda.GetDesiredPosition( chalo::NONE ), m_ayda.GetObjectCollisionRegion() ) )
    {
        // No collision!
        m_ayda.gravity.SetIsFalling( true );
    }
    else
    {
        m_ayda.gravity.SetIsFalling( false );
    }

    // Check movement
    sf::IntRect desiredPosition;
    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::A ) )
    {
        desiredPosition = m_ayda.GetDesiredPosition( chalo::WEST );
        if ( !m_map.IsCollision( desiredPosition, m_ayda.GetObjectCollisionRegion() ) )
        {
            m_ayda.Move( chalo::WEST, m_map );
        }
    }
    else if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::D ) )
    {
        desiredPosition = m_ayda.GetDesiredPosition( chalo::EAST );
        if ( !m_map.IsCollision( desiredPosition, m_ayda.GetObjectCollisionRegion() ) )
        {
            m_ayda.Move( chalo::EAST, m_map );
        }
    }
    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::W ) )
    {
        desiredPosition = m_ayda.GetDesiredPosition( chalo::NORTH );
        if ( !m_map.IsCollision( desiredPosition, m_ayda.GetObjectCollisionRegion() ) )
        {
            m_ayda.Move( chalo::NORTH, m_map );
        }
    }
    else if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::S ) )
    {
        desiredPosition = m_ayda.GetDesiredPosition( chalo::SOUTH );
        if ( !m_map.IsCollision( desiredPosition, m_ayda.GetObjectCollisionRegion() ) )
        {
            m_ayda.Move( chalo::SOUTH, m_map );
        }
    }

    // Center camera on Ayda
    m_camera.CenterOn( m_ayda.GetCenterPosition() );
}

void ClassicGameState::Draw( sf::RenderWindow& window )
{
    window.draw( m_backgroundImage );
//    m_map.Draw( window, m_camera );
    m_map.DebugDraw( window, m_camera );
    //chalo::DrawManager::AddSprite( m_backgroundImage );

    window.draw( m_ayda.GetSpriteWithOffset( m_camera ) );

    chalo::DebugDrawManager::Draw( window );
}


/* Map stuff *********************************************************/

void ClassicMoosaderMap::SetTileset( const sf::Texture& texture )
{
    m_tileset.setTexture( texture );
}

void ClassicMoosaderMap::LoadFile( const std::string& mapPath, const std::string& filename, const sf::Texture& texture )
{
    SetTileset( texture );

    std::ifstream input( mapPath + filename );
    std::string buffer;
    int mapWidth, mapHeight;

    input >> buffer; // x
    input >> mapWidth;
    input >> buffer; // y
    input >> mapHeight;
    input >> buffer >> buffer; // bgimage none

    m_width = 75;
    m_height = 56;
    m_tileWidthHeight = 32;

    // Tile time :O
    int x = 0;
    int y = 0;
    int z = 0;

    int tileCode;

    while ( input >> tileCode )
    {
        m_mapGrid[x][y][z] = tileCode;

        x++;
        if ( x >= m_width )
        {
            x = 0;
            y++;

            if ( y >= m_height )
            {
                y = 0;
                z++;
            }
        }
    }
    m_layers = z+1;
}

void ClassicMoosaderMap::Draw( sf::RenderWindow& window )
{
    for ( int z = 0; z < m_layers; z++ )
    {
        for ( int y = 0; y < m_height; y++ )
        {
            for ( int x = 0; x < m_width; x++ )
            {
                if ( m_mapGrid[x][y][z] != 0 )
                {
                    m_tileset.setPosition( x * m_tileWidthHeight, y * m_tileWidthHeight );
                    m_tileset.setTextureRect( sf::IntRect( m_mapGrid[x][y][z] * m_tileWidthHeight, 0, 32, 32 ) );
                    window.draw( m_tileset );
                }
            }
        }
    }
}

void ClassicMoosaderMap::Draw( sf::RenderWindow& window, chalo::Camera camera )
{
    for ( int z = 0; z < m_layers; z++ )
    {
        for ( int y = 0; y < m_height; y++ )
        {
            for ( int x = 0; x < m_width; x++ )
            {
                if ( m_mapGrid[x][y][z] != 0 )
                {
                    m_tileset.setPosition( x * m_tileWidthHeight - camera.GetX(), y * m_tileWidthHeight - camera.GetY() );
                    m_tileset.setTextureRect( sf::IntRect( m_mapGrid[x][y][z] * m_tileWidthHeight, 0, 32, 32 ) );
                    window.draw( m_tileset );
                }
            }
        }
    }
}

void ClassicMoosaderMap::DebugDraw( sf::RenderWindow& window, chalo::Camera camera )
{
    Draw( window, camera );

    // Mouse hovering over any tiles?
    sf::Vector2i mousePosition = chalo::InputManager::GetMousePosition();
    mousePosition.x + camera.GetX();
    mousePosition.y + camera.GetY();

    sf::IntRect tileRect;
    tileRect.width = 32;
    tileRect.height = 32;

    for ( int z = 0; z < m_layers; z++ )
    {
        for ( int y = 0; y < m_height; y++ )
        {
            for ( int x = 0; x < m_width; x++ )
            {
                tileRect.left = x * tileRect.width;
                tileRect.top = y * tileRect.height;

                if ( m_mapGrid[x][y][z] != 0 && chalo::PointInBoxCollision( mousePosition, tileRect ) )
                {
                    // Add a label for tile information
                    sf::Text info;
                    info.setFont( chalo::FontManager::Get( "debug" ) );
                    info.setCharacterSize( 12 );
                    info.setPosition(
                        tileRect.left - camera.GetX(),
                        tileRect.top + ( z * 20 ) - camera.GetY()
                        );
                    info.setColor( sf::Color::White );
                    info.setString(
                            "[" + chalo::StringUtility::IntegerToString( x ) + "]["
                            + chalo::StringUtility::IntegerToString( y ) + "]["
                            + chalo::StringUtility::IntegerToString( z ) + "]"
                        );

                    window.draw( info );

                    // Add a grid
                    sf::RectangleShape shape;
                    shape.setPosition(
                        tileRect.left - camera.GetX(),
                        tileRect.top - camera.GetY()
                        );
                    shape.setSize( sf::Vector2f( tileRect.width, tileRect.height ) );
                    shape.setFillColor( sf::Color::Transparent );
                    shape.setOutlineColor( sf::Color::White );
                    shape.setOutlineThickness( 1 );
                    window.draw( shape );
                }
            }
        }
    }
}

void ClassicMoosaderMap::SetSolidTiles( std::vector<int> codes )
{
    m_solidTileCodes = codes;
}

bool ClassicMoosaderMap::IsCollision( sf::IntRect desiredPosition, sf::IntRect collisionRegion ) const
{
    int roughTileX = desiredPosition.left / 32;
    int roughTileY = desiredPosition.top / 32;

    int xStartRange = roughTileX - 2;
    int xEndRange = roughTileX + 2;
    if ( xStartRange < 0 ) { xStartRange = 0; }
    if ( xEndRange >= m_width ) { xEndRange = m_width - 1; }

    int yStartRange = roughTileY - 2;
    int yEndRange = roughTileY + 2;
    if ( yStartRange < 0 ) { yStartRange = 0; }
    if ( yEndRange >= m_height ) { yEndRange = m_height - 1; }

    sf::IntRect tileRect;
    tileRect.width = 32;
    tileRect.height = 32;
    int tileType;

    for ( int z = 0; z < m_layers; z++ )
    {
        for ( int y = yStartRange; y < yEndRange; y++ )
        {
            for ( int x = xStartRange; x < xEndRange; x++ )
            {
                tileRect.left = x * tileRect.width;
                tileRect.top = y * tileRect.height;
                tileType = m_mapGrid[x][y][z];

                if ( chalo::BoundingBoxCollision( desiredPosition, tileRect ) )
                {
                    // But is this a "solid" tile?
                    if ( std::find( m_solidTileCodes.begin(), m_solidTileCodes.end(), tileType ) != m_solidTileCodes.end() )
                    {
                        return true;
                    }
                }
            }
        }
    }

    return false;
}
