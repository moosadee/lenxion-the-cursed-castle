#ifndef _CLASSICGAMESTATE
#define _CLASSICGAMESTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

#include "../chalo-engine/Maps/Map.hpp"
#include "../chalo-engine/Camera/Camera.hpp"

#include <vector>
#include <stack>
#include <string>


class ClassicMoosaderMap : public chalo::Map::ReadableMap
{
    public:
    void LoadFile( const std::string& mapPath, const std::string& filename, const sf::Texture& texture );
    void SetTileset( const sf::Texture& texture );

    void Draw( sf::RenderWindow& window );
    void Draw( sf::RenderWindow& window, chalo::Camera camera );
    void DebugDraw( sf::RenderWindow& window, chalo::Camera camera );

    bool IsCollision( sf::IntRect desiredPosition, sf::IntRect collisionRegion ) const;

    void SetSolidTiles( std::vector<int> codes );

    private:
    int m_mapGrid[75][56][10];
    int m_width, m_height, m_layers;
    int m_tileWidthHeight;
    sf::Sprite m_tileset;
    std::vector<int> m_solidTileCodes;
};

class ClassicGameState : public chalo::IState
{
public:
    ClassicGameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    sf::Sprite m_backgroundImage;
    ClassicMoosaderMap m_map;
    chalo::Character m_ayda;
    chalo::Camera m_camera;
};

#endif
