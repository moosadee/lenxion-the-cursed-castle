#include "FarmingGameState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"

#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cmath>

FarmingGameState::FarmingGameState()
{
    srand( time( NULL ) );
}

void FarmingGameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "FarmingGameState::Init", "function-trace" );
    IState::Init( name );
}

void FarmingGameState::Setup()
{
    chalo::Logger::Out( "", "FarmingGameState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "background",   "Content/Classic/background.png" );
    chalo::TextureManager::Add( "tileset",      "Content/Classic/tileset.png" );
    chalo::TextureManager::Add( "ayda",         "Content/Farming/ayda32.png" );
    chalo::TextureManager::Add( "terrain",      "Content/Farming/terrain.png" );
    chalo::TextureManager::Add( "fruit",      "Content/Farming/fruit.png" );

    chalo::InputManager::Setup();

    // Make background tiles
    int tileWH = 32;
    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();

    for ( int y = 0; y < screenHeight / tileWH + 1; y++ )
    {
        for ( int x = 0; x < screenWidth / tileWH; x++ )
        {
            Terrain terrain;
            int grass = rand() % 3;
            terrain.tile.setTexture( chalo::TextureManager::Get( "terrain" ) );
            terrain.tile.setTextureRect( sf::IntRect( grass * tileWH, 0, tileWH, tileWH ) );
            terrain.tile.setPosition( x * tileWH, y * tileWH );
            terrain.countdown = 0;
            terrain.state = 0;

            m_tiles.push_back( terrain );
        }
    }

    m_player.setTexture( chalo::TextureManager::Get( "ayda" ) );
    m_player.setTextureRect( sf::IntRect( 0, 0, 32, 32 ) );
    m_playerPosition = sf::Vector2f( 0, 0 );
    m_playerDirection = chalo::SOUTH;

    m_aimShape.setSize( sf::Vector2f( 32, 32 ) );
    m_aimShape.setFillColor( sf::Color::Transparent );
    m_aimShape.setOutlineColor( sf::Color::White );
    m_aimShape.setOutlineThickness( 1 );
    SetAimRect();

    m_timeToFruit = 100;
    m_timeToGrow = 100;

    m_fruitCount.setFont( chalo::FontManager::Get( "main" ) );
    m_fruitCount.setCharacterSize( 20 );
    m_fruitCount.setPosition( 20, 20 );
    m_fruitCount.setColor( sf::Color::White );
    m_fruitCount.setString( "Fruits: 0" );
    m_totalFruits = 0;
}

void FarmingGameState::SetAimRect()
{
    sf::Vector2f position = m_player.getPosition();

    if ( m_playerDirection == chalo::NORTH )
    {
        position.y -= 32;
    }
    else if ( m_playerDirection == chalo::SOUTH )
    {
        position.y += 32;
    }
    else if ( m_playerDirection == chalo::WEST )
    {
        position.x -= 32;
    }
    else if ( m_playerDirection == chalo::EAST )
    {
        position.x += 32;
    }

    // Align to grid
    position.x = int( position.x / 32 ) * 32;
    position.y = int( position.y / 32 ) * 32;

    m_aimShape.setPosition( position );
}

void FarmingGameState::Cleanup()
{
    chalo::Logger::Out( "", "FarmingGameState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void FarmingGameState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::A ) )
    {
        MovePlayer( chalo::WEST );
    }
    else if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::D ) )
    {
        MovePlayer( chalo::EAST );
    }

    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::W ) )
    {
        MovePlayer( chalo::NORTH );
    }
    else if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::S ) )
    {
        MovePlayer( chalo::SOUTH );
    }

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Space ) )
    {
        bool success = Till();
        if ( !success )
        {
            success = Sew();
        }
    }

    for ( auto& tile : m_tiles )
    {
        if ( tile.countdown > 0 )
        {
            tile.countdown -= 1;
        }

        if ( tile.countdown == 0 && tile.state == 2 )
        {
            tile.state = 3;
            tile.countdown = m_timeToFruit;

            sf::IntRect textureRect = tile.tile.getTextureRect();
            textureRect.top = 96;
            tile.tile.setTextureRect( textureRect );
        }

        if ( tile.countdown == 0 && tile.state == 3 )
        {
            // spawn a fruit
            tile.countdown = m_timeToFruit;

            sf::Sprite fruit;
            fruit.setTexture( chalo::TextureManager::Get( "fruit" ) );
            fruit.setPosition( tile.tile.getPosition() );
            m_fruits.push_back( fruit );
        }
    }

    for ( auto& fruit : m_fruits )
    {
        sf::Vector2f pos = fruit.getPosition();
        pos.x += (rand() % 3) - 1;
        pos.y += (rand() % 3) - 1;

        if ( pos.x < 0 ) { pos.x = 0; }
        else if ( pos.x > 1280 - 32 ) { pos.x = 1280 - 32; }
        if ( pos.y < 0 ) { pos.y = 0; }
        else if ( pos.y > 720 - 32 ) { pos.y = 720 - 32; }

        fruit.setPosition( pos );
    }

    // touching fruits
    std::vector< int > removeIndices;
    for ( int i = 0; i < m_fruits.size(); i++ )
    {
        sf::Vector2f playerCenter = m_player.getPosition();
        playerCenter.x += 16;
        playerCenter.y += 16;

        sf::Vector2f fruitCenter = m_fruits[i].getPosition();
        fruitCenter.x += 16;
        fruitCenter.y += 16;

        // Distance Formula!
        float xDiff = playerCenter.x - fruitCenter.x;
        float yDiff = playerCenter.y - fruitCenter.y;
        float distance = sqrt( xDiff * xDiff + yDiff * yDiff );

        if ( distance <= 32 )
        {
            // Get rid of this fruit.
            removeIndices.push_back( i );
        }
    }

    for ( auto& index : removeIndices )
    {
        m_fruits.erase( m_fruits.begin() + index );
        m_totalFruits++;
        m_fruitCount.setString( "Fruits: " + chalo::StringUtility::IntegerToString( m_totalFruits ) );
    }

    SetAimRect();
}

bool FarmingGameState::Till()
{
    sf::IntRect aimRect;
    sf::IntRect tileRect;

    aimRect.left = m_aimShape.getPosition().x;
    aimRect.top = m_aimShape.getPosition().y;

    aimRect.width = 32;
    aimRect.height = 32;
    tileRect.width = 32;
    tileRect.height = 32;

    for ( auto& tile : m_tiles )
    {
        tileRect.left = tile.tile.getPosition().x;
        tileRect.top = tile.tile.getPosition().y;

        if ( aimRect == tileRect && tile.state == 0 && tile.countdown <= 0 )
        {
            sf::IntRect textureRect = tile.tile.getTextureRect();
            textureRect.top = 32;
            tile.tile.setTextureRect( textureRect );
            tile.state = 1;
            tile.countdown = 10;
        }
    }
}

bool FarmingGameState::Sew()
{
    sf::IntRect aimRect;
    sf::IntRect tileRect;

    aimRect.left = m_aimShape.getPosition().x;
    aimRect.top = m_aimShape.getPosition().y;

    aimRect.width = 32;
    aimRect.height = 32;
    tileRect.width = 32;
    tileRect.height = 32;

    for ( auto& tile : m_tiles )
    {
        tileRect.left = tile.tile.getPosition().x;
        tileRect.top = tile.tile.getPosition().y;

        if ( aimRect == tileRect && tile.state == 1 && tile.countdown <= 0 )
        {
            sf::IntRect textureRect = tile.tile.getTextureRect();
            textureRect.top = 64;
            tile.tile.setTextureRect( textureRect );
            tile.state = 2;
            tile.countdown = m_timeToGrow;
        }
    }
}

void FarmingGameState::MovePlayer( chalo::Direction direction )
{
    m_playerDirection = direction;
    float speed = 5;

    sf::IntRect textureRect = m_player.getTextureRect();
    if ( direction == chalo::NORTH )
    {
        m_playerPosition.y -= speed;
        textureRect.top = 32;
    }
    else if ( direction == chalo::SOUTH )
    {
        m_playerPosition.y += speed;
        textureRect.top = 0;
    }

    if ( direction == chalo::WEST )
    {
        m_playerPosition.x -= speed;
        textureRect.top = 64;
    }
    else if ( direction == chalo::EAST )
    {
        m_playerPosition.x += speed;
        textureRect.top = 96;
    }

    m_player.setTextureRect( textureRect );
}

void FarmingGameState::Draw( sf::RenderWindow& window )
{
    for ( auto& tile : m_tiles )
    {
        window.draw( tile.tile );
    }

    for ( auto& fruit : m_fruits )
    {
        window.draw( fruit );
    }

    window.draw( m_aimShape );

    sf::Vector2f playerPosGrid;

    playerPosGrid.x = int( m_playerPosition.x / 32 ) * 32;
    playerPosGrid.y = int( m_playerPosition.y / 32 ) * 32;

    m_player.setPosition( playerPosGrid );

    window.draw( m_player );
    window.draw( m_fruitCount );
}
