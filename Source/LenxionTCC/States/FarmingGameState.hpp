#ifndef _FarmingGameState
#define _FarmingGameState

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

#include "../chalo-engine/Maps/Map.hpp"

#include <vector>
#include <stack>
#include <string>

struct Terrain
{
    sf::Sprite tile;
    int state; // 0 = grass, 1 = tilled, 2 = sewed, 3 = sprouted
    int countdown;
};

class FarmingGameState : public chalo::IState
{
public:
    FarmingGameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::vector< Terrain > m_tiles;
    std::vector< sf::Sprite > m_fruits;
    sf::Text m_fruitCount;
    int m_totalFruits;
    sf::Sprite m_player;
    sf::Vector2f m_playerPosition;
    chalo::Direction m_playerDirection;
    sf::RectangleShape m_aimShape;

    float m_timeToFruit;
    float m_timeToGrow;

    void SetAimRect();
    void MovePlayer( chalo::Direction direction );
    bool Till();
    bool Sew();
};

#endif
