#include "GameState.hpp"

void GameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Initialization", "GameState:Init" );
    chalo::IState::Init( name );

    chalo::TextureManager::Add( "tileset", "Content/Graphics/Tilesets/tileset.png" );
    chalo::TextureManager::Add( "tileset-small", "Content/Graphics/Tilesets/tileset-small.png" );
    chalo::TextureManager::Add( "ayda", "Content/Graphics/Characters/spritesheet-ayda.png" );
    chalo::TextureManager::Add( "ayda-small", "Content/Graphics/Characters/spritesheet-ayda-small.png" );
    chalo::TextureManager::Add( "goblin", "Content/Graphics/Characters/spritesheet-goblin.png" );
    chalo::TextureManager::Add( "projectile", "Content/Graphics/Characters/spritesheet-projectile.png" );
    chalo::TextureManager::Add( "treasure", "Content/Graphics/Items/items.png" );
    chalo::TextureManager::Add( "window-frame", "Content/Graphics/UI/window-frame.png" );
    chalo::TextureManager::Add( "lives", "Content/Graphics/UI/lives.png" );
    chalo::FontManager::Add( "hud-main", "Content/Fonts/Space Meatball.otf" );
    chalo::FontManager::Add( "number-font", "Content/Fonts/Avara.otf" );
    chalo::FontManager::Add( "debug", "Content/Fonts/mononoki-Bold.ttf" );

    m_name = name;
    m_mapPath = "Content/Maps/";
    m_firstMap = "map";
}

void GameState::Setup()
{
    chalo::Logger::Out( "Setting up", "GameState::Setup" );

    chalo::IState::Setup();

    StartAtMap( m_firstMap );

    std::map<int, chalo::Map::TileLayer > tileLayers = m_map.GetBelowTileLayers();
    for ( auto& layer : tileLayers )
    {
        chalo::Logger::Out( "Total tiles on layer: " + chalo::StringUtility::IntegerToString( layer.second.GetTileCount() ), "GameState::Setup" );
    }

    m_hud.Init();
}

void GameState::Cleanup()
{
    chalo::Logger::Out( "", "GameState::Setup", "function-trace" );
}

void GameState::Update()
{
    // Update Player
    chalo::Action playerState = m_player.Update( m_map );

    if ( playerState == chalo::BEGIN_ATTACK )
    {
        SpawnProjectile( m_player );
    }

    // Update Projectiles
    m_removeIndices.clear();
    for ( unsigned int i = 0; i < m_projectiles.size(); i++ )
    {
        m_projectiles[i].Update( m_map );

        if ( m_projectiles[i].GetActiveState() == chalo::WAITING_DELETION )
        {
            m_removeIndices.push_back( i );
        }
    }
    CleanProjectiles();

    // Update Enemy
    m_removeIndices.clear();
    for ( unsigned int i = 0; i < m_enemies.size(); i++ )
    {
        chalo::Action enemyState = m_enemies[i].Update( m_map, m_player, m_projectiles );

        if ( enemyState == chalo::BEGIN_ATTACK )
        {
            SpawnProjectile( m_enemies[i] );
        }

        if ( m_enemies[i].GetActiveState() == chalo::WAITING_DELETION )
        {
            m_removeIndices.push_back( i );
        }
    }
    CleanEnemies();

    // Update treasure
    m_treasure.Update();

    if ( chalo::BoundingBoxCollision( m_treasure.GetObjectCollisionRegion(), m_player.GetObjectCollisionRegion() ) &&
            m_treasure.GetActiveState() != chalo::WAITING_DELETION )
    {
        // Collect treasure
        m_treasure.SetActiveState( chalo::WAITING_DELETION );
        m_hud.AddTreasure(
            chalo::TextureManager::Get( "treasure" ),
            m_treasure.GetTextureRegion()
        );
    }

    // Update map
    m_map.Update();

    // Check if player is at a warp
    if ( m_map.PlayerAtWarp( m_player.GetMapCollisionRegion() ) )
    {
        MoveToMap( m_map.GetWarpTo( m_player.GetMapCollisionRegion() ) );
    }

    // Debug stuff
    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F5 ) )
    {
        chalo::DrawManager::ToggleDebugView();
    }
}

void GameState::SpawnProjectile( const chalo::Character& shooter )
{
    sf::IntRect position = shooter.GetPositionRegion();
    chalo::Direction direction = shooter.GetDirection();

    if ( shooter.GetCharacterType() == chalo::PLAYER )
    {
        position.left += position.width / 2;
        position.top += position.height / 2;
    }

    sf::IntRect rect = sf::IntRect( 0, 0, 26, 26 );
    Projectile projectile;
    projectile.Setup( position.left, position.top, "Projectile_" + chalo::StringUtility::GetTime() );
    projectile.SetTexture( chalo::TextureManager::Get( "projectile" ), rect );
    projectile.SetDirection( direction );
    projectile.SetAnimationInformation( 1, 0 );
    projectile.SetSpeed( 10 );

    m_projectiles.push_back( projectile );
}

void GameState::CleanProjectiles()
{
    for ( int i = m_removeIndices.size() - 1; i > 0; i-- )
    {
        int removeIndex = m_removeIndices[i];

        chalo::Logger::Out( "Removing projectile #" + chalo::StringUtility::IntegerToString( removeIndex )
                            + ", named " + m_projectiles[removeIndex].GetName(), "GameState::CleanProjectiles" );
        m_projectiles.erase( m_projectiles.begin() + i );
    }
}

void GameState::CleanEnemies()
{
    for ( int i = m_removeIndices.size() - 1; i > 0; i-- )
    {
        int removeIndex = m_removeIndices[i];

        chalo::Logger::Out( "Removing enemy #" + chalo::StringUtility::IntegerToString( removeIndex )
                            + ", named " + m_enemies[removeIndex].GetName(), "GameState::CleanEnemies" );
        m_enemies.erase( m_enemies.begin() + i );
    }
}

void GameState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMap( m_map );

// Draw characters here.
    for ( auto& enemy : m_enemies )
    {
        chalo::DrawManager::AddGameObject( enemy );
    }
    chalo::DrawManager::AddGameObject( m_treasure );
    chalo::DrawManager::AddGameObject( m_player );

    for ( auto& projectile : m_projectiles )
    {
        chalo::DrawManager::AddGameObject( projectile );
    }

    m_hud.Draw();
}

/**
For when you're moving to a map from another map
*/
void GameState::MoveToMap( chalo::Map::Warp warpInfo )
{
    chalo::Logger::Out( "Move to map: " + warpInfo.GetWarpToMap(), "GameState::MoveToMap" );

    m_map.LoadTextMap( m_mapPath, warpInfo.GetWarpToMap() + ".json" );

    sf::Vector2f startingPosition = warpInfo.GetWarpToPosition();
    m_player.Setup( startingPosition.x, startingPosition.y, "Ayda" );

    chalo::Logger::Out( "Player position: " + chalo::StringUtility::CoordinateToString( startingPosition ), "GameState::StartAtMap" );

    LoadAdditionalMapInfo();
}

/**
For when this is the first map loading up
*/
void GameState::StartAtMap( const std::string& mapName )
{
    chalo::Logger::Out( "Start at map: " + mapName, "GameState::StartAtMap" );

    m_map.LoadTextMap( m_mapPath, mapName + ".moo" );

    sf::Vector2f startingPosition = m_map.GetPlayerStartingPosition();
    m_player.Setup( startingPosition.x, startingPosition.y, "Ayda" );

    chalo::Logger::Out( "Player position: " + chalo::StringUtility::CoordinateToString( startingPosition ), "GameState::StartAtMap" );


    LoadAdditionalMapInfo();
}

void GameState::LoadAdditionalMapInfo()
{

    sf::IntRect rect = sf::IntRect( 0, 0, 80, 160 );
    if ( chalo::StringUtility::StringContains( m_map.GetTilesetName(), "small" ) )
    {
        // Small graphics
        rect = sf::IntRect( 0, 0, 40, 40 );
        m_player.SetTexture( chalo::TextureManager::Get( "ayda-small" ), rect );
        m_player.SetMapCollisionRectangle( sf::IntRect( 13, 32, 14, 7 ) );
        m_player.SetObjectCollisionRectangle( sf::IntRect( 9, 6, 21, 29 ) );
    }
    else
    {
        // Big graphics
        rect = sf::IntRect( 0, 0, 80, 160 );
        m_player.SetTexture( chalo::TextureManager::Get( "ayda" ), rect );
        m_player.SetMapCollisionRectangle( sf::IntRect( 28, 140, 25, 15 ) );
        m_player.SetObjectCollisionRectangle( sf::IntRect( 22, 34, 38, 120 ) );
    }


    m_enemies.clear();
    m_treasure.SetPosition( -100, -100 );
    // TODO: Have to change for new map format
//  for ( auto& object : m_map.GetObjectInfo() )
//    {
//      sf::IntRect rect = sf::IntRect( 0, 0, 80, 160 );
//      if ( object.name == "Treasure" )
//        {
//          rect.width = 50;
//          rect.height = 50;
//
//          m_treasure.Setup( object.x, object.y, object.name );
//          m_treasure.SetTexture( TextureManager::Get( object.texture ), rect );
//          m_treasure.SetMapCollisionRectangle( sf::IntRect( 5, 5, 40, 40 ) );
//          m_treasure.SetObjectCollisionRectangle( sf::IntRect( 5, 5, 40, 40 ) );
//        }
//      else
//        {
//          Npc npc;
//          npc.Setup( int( object.x ), int( object.y ), object.name );
//          npc.SetTexture( TextureManager::Get( object.texture ), rect );
//          m_enemies.push_back( npc );
//        }
//    }
}
