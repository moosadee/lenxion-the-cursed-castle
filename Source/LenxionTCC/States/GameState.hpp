#ifndef _GAMESTATE
#define _GAMESTATE

#include "../chalo-engine/States/IState.hpp"
#include "../UI/Hud.hpp"

#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/Map.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../GameObjects/Player.hpp"
#include "../GameObjects/Npc.hpp"
#include "../GameObjects/Projectile.hpp"

#include <vector>

class GameState : public chalo::IState
{
public:
    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    void SpawnProjectile( const chalo::Character& shooter );
    void CleanProjectiles();
    void CleanEnemies();
    void MoveToMap( chalo::Map::Warp warpInfo );
    void StartAtMap( const std::string& mapName );

private:
    void LoadAdditionalMapInfo();

    chalo::Map::ReadableMap m_map;
    Hud m_hud;
    Player m_player;
    std::vector< Npc > m_enemies;
    std::vector< Projectile > m_projectiles;
    std::vector< int > m_removeIndices;
    chalo::GameObject m_treasure;
    std::string m_mapPath;
    std::string m_firstMap;
};

#endif
