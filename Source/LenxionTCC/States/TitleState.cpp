#include "TitleState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"

TitleState::TitleState()
{
}

void TitleState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "TitleState::Init", "function-trace" );
    IState::Init( name );
}

void TitleState::Setup()
{
    chalo::Logger::Out( "", "TitleState::Setup", "function-trace" );
    IState::Setup();

    ReloadAssets();
//    chalo::TextureManager::Add( "audio-level",  "Content/Graphics/UI/menu-audiolevel.png" );

    chalo::MenuManager::LoadTextMenu( "titlescreen.chalomenu" );

    chalo::InputManager::Setup();
}

void TitleState::Cleanup()
{
    chalo::Logger::Out( "", "TitleState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void TitleState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( clickedButton == "btn-quit" )
    {
        SetGotoState( "EXIT" );
    }
    else if ( clickedButton == "btn-play" )
    {
        ReloadAssets();
        chalo::MenuManager::LoadTextMenu( "titlescreen_play.chalomenu" );
    }
    else if ( clickedButton == "btn-chapter1" )
    {
        SetGotoState( "gamestate" );
    }
    else if ( clickedButton == "btn-chapter2" )
    {
        SetGotoState( "classicgamestate" );
    }
    else if ( clickedButton == "btn-minigame" )
    {
        SetGotoState( "farminggamestate" );
    }
    else if ( clickedButton == "btn-help" )
    {
        ReloadAssets();
        chalo::MenuManager::LoadTextMenu( "titlescreen_help.chalomenu" );
    }
    else if ( clickedButton == "btn-options" )
    {
        ReloadAssets();
        chalo::MenuManager::LoadTextMenu( "titlescreen_options.chalomenu" );
    }
    else if ( clickedButton == "btn-back" )
    {
        ReloadAssets();
        bool backStack = chalo::MenuManager::GoBack();
        if ( !backStack )
        {
            chalo::MenuManager::LoadTextMenu( "titlescreen.chalomenu" );
        }
    }
}

void TitleState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
}

void TitleState::ReloadAssets()
{
    // This is a workaround because something in the menu manager's LoadTextMenu
    // is causing my textures to be modified or destroyed or something, as far as I can tell.
    // The maps are still in the Texture Manager, but the textures aren't showing up.

    chalo::TextureManager::Clear();

    chalo::TextureManager::Add( "background",   "Content/Graphics/UI/titlescreen-en.png" );
    chalo::TextureManager::Add( "shade",        "Content/Graphics/UI/screen-shaded.png" );
    chalo::TextureManager::Add( "btn-play",     "Content/Graphics/UI/menubtn-play.png" );
    chalo::TextureManager::Add( "btn-help",     "Content/Graphics/UI/menubtn-help.png" );
    chalo::TextureManager::Add( "btn-options",  "Content/Graphics/UI/menubtn-options.png" );
    chalo::TextureManager::Add( "btn-quit",     "Content/Graphics/UI/menubtn-quit.png" );
    chalo::TextureManager::Add( "btn-back",     "Content/Graphics/UI/menubtn-back.png" );
    chalo::TextureManager::Add( "option-icons", "Content/Graphics/UI/menu-optionsicons.png" );
    chalo::TextureManager::Add( "btnbg-long",   "Content/Graphics/UI/menubtn-background.png" );
    chalo::TextureManager::Add( "btnbg-square", "Content/Graphics/UI/menubtn-smallsquarebackground.png" );
    chalo::TextureManager::Add( "episode-buttons", "Content/Graphics/UI/episode-buttons.png" );
}

