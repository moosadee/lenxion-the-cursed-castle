#include "Hud.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

void Hud::Init()
{
    m_x = 5;
    m_y = 18;
    m_totalTreasures = 0;
    m_inventoryGold = 0;

    // Window frame
    int wh = 85;
    int w, h;
    const sf::Texture& windowTexture = chalo::TextureManager::Get( "window-frame" );
    const sf::Texture& livesTexture = chalo::TextureManager::Get( "lives" );

    int x, y;
    for ( y = 0; y < 8; y++ )
    {
        for ( x = 0; x < 3; x++ )
        {
            sf::Sprite square;
            sf::IntRect frame;
            frame.left = x * wh;

            if ( y == 0 ) // top
            {
                frame.top = 0 * wh;
            }
            else if ( y == 7 ) // bottom
            {
                frame.top = 2 * wh;
            }
            else
            {
                frame.top = 1 * wh;
            }

            frame.width = wh;
            frame.height = wh;

            square.setPosition( x * wh + m_x, y * wh + m_y );
            square.setTexture( windowTexture );
            square.setTextureRect( frame );

            m_windowFrame.push_back( square );
        }
    }

    // Lives markers
    // TODO: Update once player lives are a thing
    int lives = 5;
    x = 0;
    y = 100;
    w = 38;
    h = 35;
    for ( int i = 0; i < lives; i++ )
    {
        x = i * (w + 9) + 15 + m_x;
        y = 20 + m_y;

        sf::Sprite life;
        life.setTexture( livesTexture );
        life.setPosition( x, y );
        life.setTextureRect( sf::IntRect( 0, 0, w, h ) );

        m_lives.push_back( life );
    }

    // Text items
    sf::Font& font = chalo::FontManager::Get( "hud-main" );
    sf::Font& font2 = chalo::FontManager::Get( "number-font" );

    sf::Text text;
    text.setFont( font );
    text.setString( "First floor" );
    text.setCharacterSize( 20 );
    text.setFillColor( sf::Color::White );
    text.setPosition( sf::Vector2f( 15 + m_x, 65 + m_y ) );

    m_text.push_back( text );

    m_textGoldCount.setFont( font2 );
    m_textGoldCount.setString( "0g" );
    m_textGoldCount.setCharacterSize( 15 );
    m_textGoldCount.setFillColor( sf::Color::White );
    m_textGoldCount.setPosition( sf::Vector2f( 15 + m_x, 90 + m_y ) );
}

void Hud::Draw()
{
    // TODO: Update for chalo changes
//    for ( auto& sprite : m_windowFrame )
//    {
//      chalo::DrawManager::AddUIImage( 0, sprite );
//    }
//    for ( auto& sprite : m_lives )
//    {
//      chalo::DrawManager::AddUIImage( 1, sprite );
//    }
//    for ( auto& text : m_text )
//    {
//      chalo::DrawManager::AddUIText( text );
//    }
//    chalo::DrawManager::AddUIText( m_textGoldCount );
//
//    for ( int y = 0; y < 3; y++ )
//      {
//        for ( int x = 0; x < 4; x++ )
//          {
//            if ( m_inventoryGrid[x][y].isActive )
//              {
//                DrawManager::AddUIImage( 1, m_inventoryGrid[x][y].sprite );
//              }
//          }
//      }
}

void Hud::AddTreasure( const sf::Texture& texture, sf::IntRect textureRect )
{
    int x = m_totalTreasures % 4;
    int y = m_totalTreasures / 4;
    m_inventoryGrid[x][y].sprite.setTexture( texture );
    m_inventoryGrid[x][y].sprite.setTextureRect( textureRect );
    m_inventoryGrid[x][y].sprite.setPosition( 30 + (x * 50), 460 + (y * 50) );
    m_inventoryGrid[x][y].isActive = true;

    m_inventoryGold += 100;
    std::string text = chalo::StringUtility::IntegerToString( m_inventoryGold ) + "g";

    m_textGoldCount.setString( text);

    m_totalTreasures++;
}
