#ifndef _HUD
#define _HUD

#include <SFML/Graphics.hpp>

#include <vector>

#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/StringUtil.hpp"

struct InventoryItem
{
    sf::Sprite sprite;
    bool isActive;
    InventoryItem()
    {
        isActive = false;
    }
};

class Hud
{
public:
    void AddTreasure( const sf::Texture& texture, sf::IntRect textureRect );
    void Init();
    void Draw();

private:
    std::vector<sf::Sprite> m_windowFrame;
    std::vector<sf::Sprite> m_lives;
    std::vector<sf::Text> m_text;
    InventoryItem m_inventoryGrid[4][3];
    int m_inventoryGold;
    int m_totalTreasures;
    sf::Text m_textGoldCount;
    int m_x, m_y;
};

#endif
