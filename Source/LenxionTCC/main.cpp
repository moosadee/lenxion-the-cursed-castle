#include <SFML/Graphics.hpp>

#include "chalo-engine/Application/Application.hpp"
#include "chalo-engine/Managers/StateManager.hpp"
#include "chalo-engine/Utilities/Logger.hpp"
#include "States/GameState.hpp"
#include "States/TitleState.hpp"
#include "States/ClassicGameState.hpp"
#include "States/FarmingGameState.hpp"

int main()
{
    chalo::Logger::Setup();

//  DrawManager::Init();

    chalo::Application::Setup( "Lenxion: The Cursed Castle (Moosader)", 1280, 720 );
    chalo::StateManager stateManager;

    // The state manager clears these.
    chalo::IState* gameState = new GameState;
    chalo::IState* classicGameState = new ClassicGameState;
    chalo::IState* titleState = new TitleState;
    chalo::IState* farmingGameState = new FarmingGameState;

    chalo::MenuManager::Setup( "Content/Menus/" );
    chalo::FontManager::Add( "main", "Content/Fonts/Railway-Semibold.otf" );
    chalo::FontManager::Add( "chapters", "Content/Fonts/Railway.otf" );
    chalo::FontManager::Add( "debug", "Content/Fonts/Railway.otf" );

    stateManager.InitManager();
    stateManager.AddState( "titlestate", titleState );
    stateManager.AddState( "gamestate", gameState );
    stateManager.AddState( "classicgamestate", classicGameState );
    stateManager.AddState( "farminggamestate", farmingGameState );
    stateManager.ChangeState( "classicgamestate" );

    while ( chalo::Application::IsRunning() )
    {
        chalo::Application::BeginDrawing();
        // Updates
        chalo::Application::Update();
        stateManager.UpdateState();

        std::string gotoState = stateManager.GetGotoState();
        if ( gotoState != "" )
        {
            if ( gotoState == "EXIT" )
            {
                chalo::Application::ReadyToQuit();
            }
            stateManager.ChangeState( stateManager.GetGotoState() );
        }

        // Drawing
        stateManager.DrawState( chalo::Application::GetWindow() );
        chalo::Application::EndDrawing();
    }

    chalo::Application::Teardown();
    chalo::Logger::Cleanup();

    return 0;
}
